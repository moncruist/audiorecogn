package com.moncruist.audiorecogn.service;

import com.moncruist.audiorecogn.model.GaussianParams;
import com.moncruist.audiorecogn.util.Matrix;
import com.moncruist.audiorecogn.util.SearchUtils;
import com.mongodb.*;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.net.UnknownHostException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: moncruist
 * Date: 18.05.13
 * Time: 10:22
 * To change this template use File | Settings | File Templates.
 */
@Stateless()
public class ClassterizationSessionBean {

    public static final int BEATS_COEFFS = 5;
    public static final int CLASSES_NUM = 3;
    public static final String[] CLASSES_NAMES = {"classical", "pop", "rock"};
    private DBCollection classPopCollection;
    private DBCollection classRockCollection;
    private DBCollection classClassicalCollection;
    private DBCollection classCollection;
    private DB db;
    private MongoClient mongo;

    private double[] meanClassical;
    private double[] meanRock;
    private double[] meanPop;

    @Inject
    private Logger log;

    @PostConstruct
    private void initDB() {
        try {
            mongo = new MongoClient();
            db = mongo.getDB("audiohash");
            classCollection = db.getCollection("classification");
            if (classCollection == null) {
                classCollection = db.createCollection("classification", null);
            }
            classRockCollection = db.getCollection("classification_rock");
            classPopCollection = db.getCollection("classification_pop");
            classClassicalCollection = db.getCollection("classification_classical");
        } catch (UnknownHostException e) {
            classCollection = null;
            classRockCollection = null;
            classPopCollection = null;
            classClassicalCollection = null;
        }
    }

    private double[][] loadAllData() {
        if(classRockCollection != null && classClassicalCollection != null && classPopCollection != null) {
            long allDataCount = classClassicalCollection.getCount() + classRockCollection.getCount() +
                    classPopCollection.getCount();

            log.log(Level.INFO, "Start loading classification data");
            double[][] allData = new double[(int)allDataCount][5];
            int idx = 0;
            int idx2 = 0;
            meanClassical = new double[BEATS_COEFFS];
            meanPop = new double[BEATS_COEFFS];
            meanRock = new double[BEATS_COEFFS];
            for(int i = 0; i < BEATS_COEFFS; i++) {
                meanClassical[i] = 0.0;
                meanPop[i] = 0.0;
                meanRock[i] = 0.0;
            }
            DBCursor cur = classClassicalCollection.find();
            while(cur.hasNext()) {
                DBObject obj = cur.next();
                ArrayList<Double> arr = (ArrayList<Double>) obj.get("beats");
                for(int i = 0; i < arr.size(); i++) {
                    if( i == BEATS_COEFFS) break;
                    allData[idx][i] = arr.get(i);
                    meanClassical[i] += arr.get(i);
                }
                idx++;
            }

            cur = classPopCollection.find();
            while(cur.hasNext()) {
                DBObject obj = cur.next();
                ArrayList<Double> arr = (ArrayList<Double>) obj.get("beats");
                for(int i = 0; i < arr.size(); i++) {
                    if( i == BEATS_COEFFS) break;
                    allData[idx][i] = arr.get(i);
                    meanPop[i] += arr.get(i);
                }
                idx++;
            }

            cur = classRockCollection.find();
            while(cur.hasNext()) {
                DBObject obj = cur.next();
                ArrayList<Double> arr = (ArrayList<Double>) obj.get("beats");
                for(int i = 0; i < arr.size(); i++) {
                    if( i == BEATS_COEFFS) break;
                    allData[idx][i] = arr.get(i);
                    meanRock[i] += arr.get(i);
                }
                idx++;
            }
            for(int i = 0; i< BEATS_COEFFS; i++) {
                meanClassical[i] /= (double)classClassicalCollection.getCount();
                meanRock[i] /= (double)classRockCollection.getCount();
                meanPop[i] /= (double)classPopCollection.getCount();
            }
            log.log(Level.INFO, "Finish loading classification data");
            return allData;
        }
        meanClassical = null;
        meanPop = null;
        meanRock = null;
        return null;
    }

    private List<GaussianParams> applyKMeans(double[][] data) {
        double[][] minMax = SearchUtils.getMinMax(data);
        double[][] vectors = new double[CLASSES_NUM][BEATS_COEFFS];


        vectors[0] = Arrays.copyOf(meanClassical, BEATS_COEFFS);
        vectors[1] = Arrays.copyOf(meanPop, BEATS_COEFFS);
        vectors[2] = Arrays.copyOf(meanRock, BEATS_COEFFS);

        boolean flag = true;

        ArrayList<GaussianParams> result = new ArrayList<GaussianParams>();

        int count = 0;
        log.log(Level.INFO, "Start KMeans");
        while(flag) {
            log.log(Level.INFO, String.format("KMeans iteration %d", count));
            for(int i = 0; i < vectors.length; i++) {
                String str = "[";
                for(int j = 0; j < vectors[i].length; j++)
                    str += Double.toString(vectors[i][j]) + " ";
                str += "]";
                log.log(Level.INFO, str);
            }
            ArrayList<ArrayList<ArrayList<Double>>> classes = new ArrayList<ArrayList<ArrayList<Double>>>();
            double[] deltas = new double[CLASSES_NUM];

            for(int k = 0; k < CLASSES_NUM; k++) {
                classes.add(new ArrayList<ArrayList<Double>>());
            }

            for(int i = 0; i < data.length; i++) {
                double[] distances = new double[CLASSES_NUM];
                double min = Double.MAX_VALUE;
                int minIdx = -1;
                for(int j = 0; j < CLASSES_NUM; j++) {
                    distances[j] = SearchUtils.distance(vectors[j], data[i], minMax);
                    if(distances[j] < min) {
                        min = distances[j];
                        minIdx = j;
                    }
                }
                ArrayList<Double> arr = new ArrayList<Double>();
                for(int j = 0; j < BEATS_COEFFS; j++) {
                    arr.add(data[i][j]);
                }

                classes.get(minIdx).add(arr);
            }

            double deltasMax = Double.MIN_VALUE;

            for(int k = 0; k < CLASSES_NUM; k++) {
                double[] average = new double[BEATS_COEFFS];
                for(int j = 0; j < BEATS_COEFFS; j++) {
                    average[j] = 0.0;
                }

                ArrayList<ArrayList<Double>> klass = classes.get(k);
                for(int i = 0; i < klass.size(); i++) {
                    for(int j = 0; j < BEATS_COEFFS; j++) {
                        average[j] += klass.get(i).get(j);
                    }
                }

                for(int j = 0; j < BEATS_COEFFS; j++) {
                    average[j] /= (double)klass.size();
                }

                deltas[k] = SearchUtils.distance(average, vectors[k], minMax);
                log.log(Level.INFO, String.format("Delta %d %f", k, deltas[k]));
                if(deltas[k] > deltasMax)
                    deltasMax = deltas[k];

                vectors[k] = Arrays.copyOf(average, BEATS_COEFFS);
            }

            if(deltasMax <= 0.00001) {
                flag = false;
                log.log(Level.INFO, "Needed delta reached. Starting calculation gaus params");
                for(int i = 0; i < CLASSES_NUM; i++) {
                    result.add(calcGaussianParams(classes.get(i), data.length));
                    result.get(i).setName(CLASSES_NAMES[i]);
                    //result.get(i).setMinMax(minMax);
                }
                log.log(Level.INFO, "Finish calculation gaus params");
            }

            count++;
        }
        return result;
    }

    private GaussianParams calcGaussianParams(ArrayList<ArrayList<Double>> klass, int allDataCount) {
        if(klass == null || klass.size() == 0)
            return null;

        double[][] allData = new double[klass.size()][BEATS_COEFFS];
        for(int i = 0; i < klass.size(); i++) {
            for(int j = 0; j < BEATS_COEFFS; j++)
                allData[i][j] = klass.get(i).get(j);
        }

        int vectorLength = klass.get(0).size();
        double[] means = new double[vectorLength];
        for(int i = 0; i < vectorLength; i++) {
            means[i] = 0.0;
        }

        for(int i = 0; i < klass.size(); i++) {
            for(int j = 0; j < vectorLength; j++) {
                means[j] += klass.get(i).get(j);
            }
        }
        for(int j = 0; j < vectorLength; j++) {
            means[j] /= klass.size();
        }
        double[][] norm = new double[klass.size()][vectorLength];
        for(int i = 0; i < klass.size(); i++) {
            for(int j = 0; j < vectorLength; j++) {
                norm[i][j] = klass.get(i).get(j) - means[j];
            }
        }

        double[][] variances = new double[vectorLength][vectorLength];
        for(int i = 0; i < vectorLength; i++) {
            for(int j = 0; j < vectorLength; j++) {
                double avg = 0.0;
                for(int k = 0; k < klass.size(); k++) {
                    avg += norm[k][i] * norm[k][j];
                }
                avg /= (double)klass.size();
                variances[i][j] = avg;
            }
        }

        GaussianParams result = new GaussianParams();
        result.setDensity(vectorLength);
        result.setMeans(means);
        result.setVariances(variances);
        result.setWeight((double)klass.size() / (double)allDataCount);
        result.setDeterminant(Matrix.determinant(variances));
        double[][] inv = new double[vectorLength][vectorLength];
        for(int i = 0; i < vectorLength; i++) {
            for(int j = 0; j < vectorLength; j++) {
                inv[i][j] = variances[i][j];
            }
        }
        Matrix.invert(inv);
        result.setInvertedVariances(inv);
        result.setMinMax(SearchUtils.getMinMax(allData));

        return result;

    }

    public void classificateTestData() {
        log.log(Level.INFO, "Starting classification");
        double[][] allData = loadAllData();
        List<GaussianParams> gaus = applyKMeans(allData);
        classCollection.drop();
        classCollection = db.createCollection("classification", null);
        for(GaussianParams g : gaus) {
            classCollection.insert(g.toDBObject());
        }
        log.log(Level.INFO, "Finish classification");
    }

    public double applyGaussian(double[] vector, GaussianParams params) {
//        if(vector.length != params.getDensity())
//            return 0.0;
//
//        log.log(Level.INFO, "Step1 reached");
//
//        for(int i = 0; i < vector.length; i++) {
//            String str = "vector [";
//            str += Double.toString(vector[i]) + " ";
//            str += "]";
//            log.log(Level.INFO, str);
//        }
//
//        double[] means = params.getMeans();
//        double[] norm = new double[means.length];
//        for(int i = 0; i < means.length; i++) {
//            norm[i] = vector[i] - means[i];
//        }
//
//        for(int i = 0; i < norm.length; i++) {
//            String str = "Norm [";
//            str += Double.toString(norm[i]) + " ";
//            str += "]";
//            log.log(Level.INFO, str);
//        }
//
//        double[][] inv = params.getInvertedVariances();
//
//        double sum = 0.0;
//        for(int i = 0; i < means.length; i++) {
//                sum += norm[i] * inv[i][i] * norm[i];
//        }
//        log.log(Level.INFO, String.format("Sum1 %f", sum));
//        sum *= -0.5;
//
//        log.log(Level.INFO, String.format("Sum2 %f", sum));
//        double power = ((double) vector.length) / 2.0;
//        double twoPi = Math.pow(2.0 * Math.PI, power);
//        double detSqrt = Math.sqrt(params.getDeterminant());
//        double mult = 1.0 / (twoPi * detSqrt);
//
//        log.log(Level.INFO, String.format("Mult %f", mult));
//        double exp = Math.exp(sum);
//        log.log(Level.INFO, String.format("Exp %f", exp));
//        double result = mult * exp;
//        result *= params.getWeight();

        double result = SearchUtils.distance(vector, params.getMeans(), params.getMinMax());
        return result;
    }

    public Map<String, Double> classificateVector(double[] vector) {
        HashMap<String, Double> result = new HashMap<String, Double>();
        DBCursor cursor = classCollection.find();
        int idx = 0;
        while(cursor.hasNext()) {
            DBObject obj = cursor.next();
            GaussianParams g = GaussianParams.fromDBObject(obj);
            result.put(CLASSES_NAMES[idx], applyGaussian(vector, g));
            idx++;
        }
        return result;
    }

    public void reloadClassificationIfNeed() {
        if(classCollection == null || classCollection.count() < CLASSES_NUM) {
            classificateTestData();
        }
    }

    @PreDestroy
    public void closeDB() {
        mongo.close();
    }
}
