package com.moncruist.audiorecogn.service;

import com.moncruist.audiorecogn.model.Match;
import com.moncruist.audiorecogn.model.MatchComparator;
import com.moncruist.audiorecogn.util.SearchUtils;
import com.mongodb.*;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.net.UnknownHostException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: moncruist
 * Date: 27.04.13
 * Time: 20:15
 * To change this template use File | Settings | File Templates.
 */
@Stateless
public class AudioSearchSessionBean {

    private DBCollection musicCollection;

    @Inject
    private Logger log;

    @EJB
    private ClassterizationSessionBean classterization;

    public ClassterizationSessionBean getClassterization() {
        return classterization;
    }

    public void setClassterization(ClassterizationSessionBean classterization) {
        this.classterization = classterization;
    }

    @PostConstruct
    private void initDB() {
        try {
            MongoClient mongo = new MongoClient();
            DB db = mongo.getDB("audiohash");
            musicCollection = db.getCollection("music");
            if (musicCollection == null) {
                musicCollection = db.createCollection("music", null);
            }
        } catch (UnknownHostException e) {
            musicCollection = null;
        }
    }

    public List<Match> searchMatch(double[][] hash, double[] beats) {
        if (musicCollection != null) {
            ArrayList<Match> matches = new ArrayList<Match>();

            classterization.reloadClassificationIfNeed();
            Map<String, Double> clresult = classterization.classificateVector(beats);
            Set<String> keys = clresult.keySet();
            for(String k : keys) {
                log.log(Level.INFO, String.format("%s %f", k, clresult.get(k)));
            }

            double[][] min_max = SearchUtils.getMinMax(hash);
            DBCursor cursor = musicCollection.find();
            int hash_len = hash.length;

//            for(int i = 0; i < hash_len; i++) {
//                String str = "[";
//                for(int j = 0; j < hash[i].length; j++)
//                    str += Double.toString(hash[i][j]) + " ";
//                str += "]";
//                log.log(Level.INFO, str);
//            }

            log.log(Level.INFO, String.format("Hash length %d", hash_len));
            log.log(Level.INFO, "Search start");
            int idx = 0;
            while (cursor.hasNext()) {
                //log.log(Level.INFO, String.format("Search idx %d", idx));
                DBObject dbo = cursor.next();
                Match newMatch = Match.fromDBObject(dbo);
                ArrayList arr = (ArrayList) dbo.get("hash");


                double[][] music = new double[arr.size()][hash[0].length];
                for(int i = 0; i < arr.size(); i++) {
                    ArrayList<Double> arr2 = (ArrayList<Double>)arr.get(i);
                    for(int j = 0; j < arr2.size(); j++)
                        music[i][j] = arr2.get(j);
                }
                int music_len = music.length;


//                if(newMatch.getTitle().equals("Tie You Up (The Pain Of Love)")) {
//                log.log(Level.INFO, String.format("Music length %d", music_len));
//                for(int i = 0; i < music_len; i++) {
//                    String str = "[";
//                    for(int j = 0; j < music[i].length; j++)
//                        str += Double.toString(music[i][j]) + " ";
//                    str += "]";
//                    log.log(Level.INFO, str);
//                }
//                }

                double score = Double.MAX_VALUE;
                for(int i = 0; i < music_len - hash_len; i++) {
                    double sum = 0.0;
                    for(int j = 0; j < hash_len; j++) {
                        sum += SearchUtils.distance(music[i+j], hash[j], min_max);
                    }
                    if(sum < score)
                        score = sum;
                }

                newMatch.setScore(score);
                newMatch.setClassScore(clresult);
                if(newMatch.getTitle().equals("Tie You Up (The Pain Of Love)"))
                    log.log(Level.INFO, String.format("Tie You Up (The Pain Of Love) match %f", newMatch.getScore()));
                if(matches.size() < 10) {
                    matches.add(newMatch);
                    Collections.sort(matches, new MatchComparator());
                }
                else {
                    matches.add(newMatch);
                    Collections.sort(matches, new MatchComparator());
                    matches.remove(10);
                }
                idx++;
            }
            log.log(Level.INFO, "Search end");
            return matches;
        } else {
            return new ArrayList<Match>();
        }
    }

    public List<Match> getAllMusicList() {
        if (musicCollection != null) {
            ArrayList<Match> matches = new ArrayList<Match>();
            DBCursor cursor = musicCollection.find();
            while(cursor.hasNext()) {
                DBObject obj = cursor.next();
                Match m = Match.fromDBObject(obj);
                m.setScore(-1.0);
                m.setClassScore(null);
                matches.add(m);
            }
            return matches;
        } else {
            return new ArrayList<Match>();
        }
    }
}
