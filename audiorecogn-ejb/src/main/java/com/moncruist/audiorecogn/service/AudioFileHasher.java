package com.moncruist.audiorecogn.service;

import com.moncruist.audiohash.AudioHashWrapper;

import javax.ejb.Stateless;

/**
 * Created with IntelliJ IDEA.
 * User: moncruist
 * Date: 21.04.13
 * Time: 18:02
 * To change this template use File | Settings | File Templates.
 */
@Stateless
public class AudioFileHasher {
    public double[][] hashFile(String fileName) {
        return AudioHashWrapper.calcSpectralHash(fileName);
    }

    public double[] calcBeats(String fileName) {
        return AudioHashWrapper.calcBeatHistogram(fileName);
    }
}
