package com.moncruist.audiorecogn.model;

import java.util.Comparator;

/**
 * Created with IntelliJ IDEA.
 * User: moncruist
 * Date: 28.04.13
 * Time: 13:12
 * To change this template use File | Settings | File Templates.
 */
public class MatchComparator implements Comparator<Match> {
    @Override
    public int compare(Match o1, Match o2) {
        if(o1.getScore() > o2.getScore())
            return 1;
        else if(o1.getScore() < o2.getScore())
            return -1;
        else
            return 0;
    }
}
