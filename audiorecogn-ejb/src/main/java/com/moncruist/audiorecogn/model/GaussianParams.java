package com.moncruist.audiorecogn.model;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: moncruist
 * Date: 18.05.13
 * Time: 19:49
 * To change this template use File | Settings | File Templates.
 */
public class GaussianParams {
    private double weight;
    private int density;
    private double[] means;
    private double[][] variances;
    private double determinant;
    private double[][] invertedVariances;
    private double[][] minMax;

    public double[][] getMinMax() {
        return minMax;
    }

    public void setMinMax(double[][] minMax) {
        this.minMax = minMax;
    }

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getDeterminant() {
        return determinant;
    }

    public void setDeterminant(double determinant) {
        this.determinant = determinant;
    }

    public double[][] getInvertedVariances() {
        return invertedVariances;
    }

    public void setInvertedVariances(double[][] invertedVariances) {
        this.invertedVariances = invertedVariances;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public int getDensity() {
        return density;
    }

    public void setDensity(int density) {
        this.density = density;
    }

    public double[] getMeans() {
        return means;
    }

    public void setMeans(double[] means) {
        this.means = means;
    }

    public double[][] getVariances() {
        return variances;
    }

    public void setVariances(double[][] variances) {
        this.variances = variances;
    }

    public static ArrayList<ArrayList<Double>> varianceToList(double[][] variance) {
        ArrayList<ArrayList<Double>> result = new ArrayList<ArrayList<Double>>();
        for(double[] var : variance) {
            ArrayList<Double> arr = new ArrayList<Double>();
            for(double d : var) {
                arr.add(d);
            }
            result.add(arr);
        }
        return result;
    }

    public static double[][] listToVariance(ArrayList<ArrayList<Double>> list) {

        if(list == null || list.size() == 0)
            return null;

        int n = list.size();

        double[][] result = new double[n][n];
        for(int i = 0; i < n; i++) {
            for(int j = 0; j < n; j++) {
                result[i][j] = list.get(i).get(j);
            }
        }

        return result;
    }

    public static double[][] listToMinMax(ArrayList<ArrayList<Double>> list) {

        if(list == null || list.size() == 0)
            return null;

        int n = list.size();

        double[][] result = new double[2][5];
        for(int i = 0; i < 2; i++) {
            for(int j = 0; j < 5; j++) {
                result[i][j] = list.get(i).get(j);
            }
        }

        return result;
    }

    public static ArrayList<ArrayList<Double>> minMaxToList(double[][] minMax) {
        ArrayList<ArrayList<Double>> result = new ArrayList<ArrayList<Double>>();
        for(double[] var : minMax) {
            ArrayList<Double> arr = new ArrayList<Double>();
            for(double d : var) {
                arr.add(d);
            }
            result.add(arr);
        }
        return result;
    }

    public static ArrayList<Double> meansToList(double[] means) {
        ArrayList<Double> result = new ArrayList<Double>();
        for(double d : means) {
            result.add(d);
        }
        return result;
    }

    public static double[] listToMeans(ArrayList<Double> list) {
        if(list == null)
            return null;
        double[] result = new double[list.size()];
        for(int i = 0; i < list.size(); i++) {
            result[i] = list.get(i);
        }
        return result;
    }

    @SuppressWarnings("unchecked")
    public static GaussianParams fromDBObject(DBObject document) {
        GaussianParams g = new GaussianParams();
        g.weight = (Double)document.get("weight");
        g.density = (Integer)document.get("density");
        g.determinant = (Double)document.get("determinant");
        g.name = (String)document.get("name");
        g.means = listToMeans( (ArrayList<Double>)document.get("means"));
        g.variances = listToVariance((ArrayList<ArrayList<Double>>)document.get("variances"));
        g.invertedVariances = listToVariance((ArrayList<ArrayList<Double>>)document.get("inverted"));
        g.minMax = listToMinMax((ArrayList<ArrayList<Double>>)document.get("minmax"));

        return g;
    }

    public BasicDBObject toDBObject() {
        BasicDBObject doc = new BasicDBObject();
        doc.put("weight", weight);
        doc.put("density", density);
        doc.put("determinant", determinant);
        doc.put("name", name);
        doc.put("means", meansToList(means));
        doc.put("variances", varianceToList(variances));
        doc.put("inverted", varianceToList(invertedVariances));
        doc.put("minmax", minMaxToList(minMax));

        return doc;
    }
}
