package com.moncruist.audiorecogn.model;

import com.mongodb.DBObject;

import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: moncruist
 * Date: 28.04.13
 * Time: 12:56
 * To change this template use File | Settings | File Templates.
 */
public class Match {
    private String title;
    private String author;
    private String genre;
    private double score;
    private Map<String, Double> classScore;

    public Map<String, Double> getClassScore() {
        return classScore;
    }

    public void setClassScore(Map<String, Double> classScore) {
        this.classScore = classScore;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    public static Match fromDBObject(DBObject document) {
        Match m = new Match();

        m.author = (String) document.get("author");
        m.title = (String) document.get("title");
        m.genre = (String) document.get("genre");
        m.score = -1;

        return m;
    }
}
