package com.moncruist.audiorecogn.util;

/**
 * Created with IntelliJ IDEA.
 * User: moncruist
 * Date: 27.04.13
 * Time: 20:24
 * To change this template use File | Settings | File Templates.
 */
public class SearchUtils {
    public static double[][] getMinMax(double[][] hash) {
        if(hash.length > 0) {
            int hash_length = hash[0].length;
            double[][] result = new double[2][hash[0].length];
            for(int i = 0; i < hash_length; i++) {
                result[0][i] = Integer.MAX_VALUE;
                result[1][i] = Integer.MIN_VALUE;
            }

            for(double[] val : hash) {
                for(int i = 0; i < hash_length; i++) {
                    if(val[i] < result[0][i])
                        result[0][i] = val[i];
                    if(val[i] > result[1][i])
                        result[1][i] = val[i];
                }
            }
            return result;
        } else {
            return null;
        }
    }

    public static double distance(double[] a, double[] b, double[][] minMax) {
        double sum = 0.0;
        if(a.length != b.length)
            return sum;
        for(int i = 0; i < a.length; i++) {
            sum += Math.abs(a[i] - b[i]) / (minMax[1][i] - minMax[0][i]);
        }
        return sum;
    }


    public static double euclidean(double[] a, double[] b) {
        double sum = 0.0;
        if(a.length != b.length)
            return sum;
        for(int i = 0; i < a.length; i++) {
            double dist = a[i] - b[i];
            sum += dist*dist;
        }

        return Math.sqrt(sum);
    }
}
