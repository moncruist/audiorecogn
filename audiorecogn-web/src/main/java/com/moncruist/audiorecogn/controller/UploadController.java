package com.moncruist.audiorecogn.controller;

import com.moncruist.audiorecogn.model.Match;
import com.moncruist.audiorecogn.service.AudioFileHasher;
import com.moncruist.audiorecogn.service.AudioSearchSessionBean;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.myfaces.custom.fileupload.DiskStorageStrategy;
import org.apache.myfaces.custom.fileupload.UploadedFile;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: moncruist
 * Date: 21.04.13
 * Time: 12:04
 * To change this template use File | Settings | File Templates.
 */
@RequestScoped
@ManagedBean
@Named
public class UploadController {

    private UploadedFile uploadedFile;

    private Logger log = Logger.getLogger(UploadController.class.getName());

    private double[][] hash;

    private List<Match> bestMatch;

    public List<Match> getBestMatch() {
        return bestMatch;
    }

    public void setBestMatch(List<Match> bestMatch) {
        this.bestMatch = bestMatch;
    }

    public double[][] getHash() {
        return hash;
    }

    public void setHash(double[][] hash) {
        this.hash = hash;
    }

    @EJB
    private AudioFileHasher hasher;

    @EJB
    private AudioSearchSessionBean searcher;

    @Inject
    private FacesContext facesContext;

    public String submitFile() {
        String fileName = uploadedFile.getName();

        log.log(Level.INFO, String.format("Uploaded file %s", fileName));

        // Проверяем на соответствие типа файла
        if(!uploadedFile.getContentType().equals("audio/wav")) {
            log.log(Level.WARNING, String.format("Uploaded file %s have content type %s which differs from audio/wav",
                    fileName, uploadedFile.getContentType()));
            facesContext.addMessage("upload", new FacesMessage(String.format("Uploaded file %s have content type %s which differs from audio/wav",
                    fileName, uploadedFile.getContentType())));

            return null;
        }

        DiskStorageStrategy strategy = (DiskStorageStrategy)uploadedFile.getStorageStrategy();
        File file = strategy.getTempFile();
        log.log(Level.INFO, "Start hashing");
        log.log(Level.INFO, file.getAbsolutePath());
        hash = hasher.hashFile(file.getAbsolutePath());
        double[] beats = hasher.calcBeats(file.getAbsolutePath());
        log.log(Level.INFO, "Complete hashing");

        log.log(Level.INFO, String.format("Deleting file %s", file.getAbsolutePath()));
        if(!file.delete()) {
            log.log(Level.WARNING, String.format("Cannot delete file %s", file.getAbsolutePath()));
        }

        log.log(Level.INFO, String.format("Hash length %d", hash.length));

        bestMatch = searcher.searchMatch(hash, beats);

        return "hashlist.xhtml";
    }

    public UploadedFile getUploadedFile() {
        return uploadedFile;
    }

    public void setUploadedFile(UploadedFile uploadedFile) {
        this.uploadedFile = uploadedFile;
    }
}
