package com.moncruist.audiorecogn.controller;

import com.moncruist.audiorecogn.model.Match;
import com.moncruist.audiorecogn.service.AudioSearchSessionBean;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;
import javax.inject.Named;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: moncruist
 * Date: 26.05.13
 * Time: 22:57
 * To change this template use File | Settings | File Templates.
 */
@RequestScoped
@ManagedBean
@Named
public class MusicListController {

    private Logger log = Logger.getLogger(UploadController.class.getName());

    private List<Match> list;

    public List<Match> getList() {
        return list;
    }

    public void setList(List<Match> list) {
        this.list = list;
    }

    @EJB
    private AudioSearchSessionBean searcher;

    public void getAllMusic() {
        log.log(Level.INFO, "Getting all music list");
        list = searcher.getAllMusicList();
    }
}
